//
// Created by root on 12.08.21.
//

#ifndef CPP_SERVER_LOCAL_LOGGER_H
#define CPP_SERVER_LOCAL_LOGGER_H
//
// Created by user on 2.08.21.
//

#include <iostream>
#include <vector>
#include <fstream>
#include <array>
#include <thread>
#include <mutex>

#define STR_(x) #x
#define STR(x) STR_(x)

#define LOG_TRACE(message) globalLogger().log(Logger::Logger::LVL::TRACE, (message))
#define LOG_DEBUG(message) globalLogger().log(Logger::Logger::LVL::DEBUG, (message))
#define LOG_INFO(message)  globalLogger().log(Logger::Logger::LVL::INFO, (message))
#define LOG_ERROR(message) globalLogger().log(Logger::Logger::LVL::ERROR, (message))
#define LOG_FATAL(message) globalLogger().log(Logger::Logger::LVL::FATAL, (message))
#define LOG_WARNING(message) globalLogger().log(Logger::Logger::LVL::WARNING, (message))

namespace Logger
{
    class LoggerWorker
            {
            public:
                LoggerWorker(std::string path, std::size_t buffer_size = 4096u) :
                _outfile(path),
                _vbuff(buffer_size),
                _offset(0u)
                {};
            public:
                void write(std::string& data);
                void flush();

            private:
                std::ofstream       _outfile;
                std::size_t         _offset;
                std::vector<char>   _vbuff;
            };


    class Logger
            {
            public:
                enum LVL
                        {
                    TRACE,
                    DEBUG,
                    INFO,
                    FATAL,
                    ERROR,
                    WARNING
                        };

                std::array<std::string, LVL::WARNING + 1u> error_lvl = {
                        "TRACE", "DEBUG", "INFO", "FATAL", "ERROR", "WARNING"
                };

                void log(LVL lvl,const std::string& message);

            public:
                Logger(std::string path) :
                _loggerWorker(path)
                {};

            private:
                LoggerWorker _loggerWorker;
            };

}

inline Logger::Logger& globalLogger()
{
    static Logger::Logger logger("C:\\Users\\dimma\\Desktop\\Project1\\Project1\\logger_data.log");
    return logger;
}
#endif //CPP_SERVER_LOCAL_LOGGER_H
