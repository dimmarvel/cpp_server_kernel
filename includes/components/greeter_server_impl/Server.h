//
// Created by user on 15.07.21.
//

#ifndef CPP_SERVER_SERVER_H
#define CPP_SERVER_SERVER_H

#include "message_kernel.grpc.pb.h"
#include <iostream>
#include <memory>
#include <string>
#include <local_log/local_logger.h>
#include <grpc++/grpc++.h>
#include <user.h>
#include <rapidjson/document.h>

namespace net {

    using grpc::Server;
    using grpc::ServerBuilder;
    using grpc::ServerContext;
    using grpc::Status;

    using UserApi::ServerInfo;
    using UserApi::PersonalData;
    using UserApi::User;

    using UserApi::AuthorizationDataRequest;
    using UserApi::AuthorizationDataResponse;
    using UserApi::Authorization;
    using UserApi::RegistrationDataRequest;
    using UserApi::RegistrationDataResponse;
    using UserApi::Registration;

    class AuthorizationServiceImpl final : public Authorization::Service {
        Status LogIn(ServerContext* context, const AuthorizationDataRequest* request,
                     AuthorizationDataResponse* response) override;
    };

    class RegistrationServiceImpl final : public Registration::Service {
        Status RegistrationNewUser(ServerContext *context, const RegistrationDataRequest *request,
                                   RegistrationDataResponse *response) override;
    };

    class ServerKernel {
    private:
        int _port;
        std::string _ipv4;
        std::string _server_address;
        ServerBuilder _builder;

    public:
        ServerKernel(std::string ipv4, int port);
        ServerKernel();

    public:
        void start();
    };
}


#endif //CPP_SERVER_SERVER_H
