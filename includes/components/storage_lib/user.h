//
// Created by root on 16.08.21.
//

#ifndef CPP_SERVER_USER_H
#define CPP_SERVER_USER_H

#include <iostream>
#include <string>
#include <storage.h>

namespace user {

    enum AccessLvl {
        LOW = 0,
        AVERAGE = 1,
        HIGH = 2
    };

    class PersonalData {
    public:
        PersonalData(std::string _name, std::string _surname, std::string _patronumic, std::string _yearsold)
        : name(_name),
        surname(_surname),
        patronumic(_patronumic),
        yearsold(_yearsold){};

        PersonalData(PersonalData& pd)
        : name(pd.name),
        surname(pd.surname),
        patronumic(pd.patronumic),
        yearsold(pd.yearsold){};

        PersonalData() {};
    public:
        std::string name;
        std::string surname;
        std::string patronumic;
        std::string yearsold;
    };

    class User : public bd::StorageHandler {
        public:
            User(std::string login, std::string pass, AccessLvl accessLvl, PersonalData pd);
            User(const User &user);
            User(User &user);

        public:
            bool AddInDb() override;
            bool RemoveFromDb() override;

            PersonalData* get_personalData();
            std::string get_login();
            std::string get_pass();
            int get_id();
            AccessLvl get_access_lvl();
            void show_data();

        private:
            int _id;
            std::string _login;
            std::string _password;
            AccessLvl _accessLvl;
            PersonalData* _personalData;
        };

    bool UpdateInDb(User& current_usr, User& update_usr);
}

#endif //CPP_SERVER_USER_H
