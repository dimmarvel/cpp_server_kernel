//
// Created by root on 12.08.21.
//

#include <local_logger.h>

namespace Logger
{
    std::string current_time() {
        time_t now = time(0);
        tm *ltm = localtime(&now);
        std::string time_str =
                std::to_string(ltm->tm_hour) + ":" +
                std::to_string(ltm->tm_min) + ":" +
                std::to_string(ltm->tm_sec);
        return time_str;
    }
    //--------------------------LoggerWorker init--------------------------
    void LoggerWorker::write(std::string &data) {
        const std::size_t data_size = data.size();

        if ((data_size + _offset) >= _vbuff.size())
            flush();

        std::copy(begin(data), end(data), begin(_vbuff) + _offset);
        _offset += data_size;
    }

    void LoggerWorker::flush() {
        _outfile.write(_vbuff.data(), _offset);
        _offset = 0u;
    }
    //-----------------------------------------------------------------------
    //--------------------------Logger init----------------------------------
    void Logger::log(LVL lvl, const std::string &message) {
        std::string format_str = current_time() + " [" + error_lvl[lvl] + "] " + message + "\n";
        std::cout << format_str;
        _loggerWorker.write(format_str);
    }


}