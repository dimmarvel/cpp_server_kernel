//
// Created by user on 15.07.21.
//

#include <greeter_server_impl/Server.h>

namespace net
{

    ServerKernel::ServerKernel(std::string ipv4, int port) {
        _port = port;
        _ipv4 = ipv4;
        _server_address = _ipv4 + ":" + std::to_string(_port);
    }

    ServerKernel::ServerKernel() :
        _port(8080),
        _ipv4("127.0.0.1"),
        _server_address(_ipv4 + ":" +  std::to_string(_port)) {
        LOG_INFO("Create default server address: " + _server_address);

    };

    void ServerKernel::start() {
        AuthorizationServiceImpl authorizationService;
        RegistrationServiceImpl registrationService;
        _builder.AddListeningPort(_server_address, grpc::InsecureServerCredentials());
        _builder.RegisterService(&authorizationService);
        _builder.RegisterService(&registrationService);
        std::unique_ptr<Server> server(_builder.BuildAndStart());
        LOG_INFO("Server listening on -> " + _server_address);
        LOG_INFO("Server waiting request...");
        server->Wait();
    }

    // ---------------------------------------------------------------------------------------

    Status AuthorizationServiceImpl::LogIn(ServerContext *context, const AuthorizationDataRequest *request,
                                           AuthorizationDataResponse *response)
    {
        mongocxx::collection collection = bd::Storage::get_instance()->get_db()[bd::kCollectionName.c_str()];
        PersonalData* personalData = new PersonalData();
        ServerInfo* serverInfo = new ServerInfo();
        User* user = new User();
        rapidjson::Document document;

        LOG_INFO("------------> Login in...");

        if(request->login() == "" || request->password() == "") {
            LOG_ERROR("-------------> Empty data from client.");
            serverInfo->set_message("[SERVER] -> Error: login or password is empty.");
            response->set_isauth(false);
            return Status::OK;
        }

        auto result = collection.find_one(bsoncxx::builder::stream::document{}
                                        << "login" << request->login()
                                        << "password" << request->password()
                                        << bsoncxx::builder::stream::finalize);
        document.Parse(bsoncxx::to_json(result->view()).c_str());

        if(result) {

            personalData->set_name(document["personal_data"]["name"].GetString());
            personalData->set_surname(document["personal_data"]["surname"].GetString());
            personalData->set_patronymic(document["personal_data"]["patronumic"].GetString());
            personalData->set_yearsold(document["personal_data"]["yearsold"].GetString());

            user->set_login(document["login"].GetString());
            user->set_password(document["password"].GetString());
            user->set_access_lvl((UserApi::User_AccessLvl)document["access_lvl"].GetInt64());
            user->set_allocated_personal_data(personalData);

            response->set_allocated_user(user);
            response->set_isauth(true);

            LOG_INFO("------------> Success authorization!");
            serverInfo->set_message("[SERVER] -> Success authorization.");

        } else {
            LOG_ERROR("-------------> Canceled login in, user not found!");
            serverInfo->set_message("[SERVER] -> Error: Canceled login in, user not found");
            response->set_isauth(false);
            return Status::OK;
        }

        response->set_allocated_msg(serverInfo);

        return Status::OK;
    }

    Status RegistrationServiceImpl::RegistrationNewUser(ServerContext *context, const RegistrationDataRequest *request,
                               RegistrationDataResponse *response)
    {
        ServerInfo* serverInfo = new ServerInfo();
        User* user_impl = new User();
        PersonalData* personalData = new PersonalData();

        LOG_INFO("-------------> Start registration... ");
        user::PersonalData pd(request->user_reg_data().personal_data().name(),
                            request->user_reg_data().personal_data().surname(),
                            request->user_reg_data().personal_data().patronymic(),
                            request->user_reg_data().personal_data().yearsold());

        user::User usr(request->user_reg_data().login(),
                  request->user_reg_data().password(),
                  (user::AccessLvl)request->user_reg_data().access_lvl(),
                  pd);

        if(usr.AddInDb() == true) {
            serverInfo->set_message("[SERVER] -> Success register " +
                    request->user_reg_data().personal_data().name());
        } else {
            serverInfo->set_message("[SERVER] -> Error: Canceled register, user already exist");
            response->set_allocated_msg(serverInfo);
            response->set_isregister(false);
            return Status::OK;
        }


        personalData->set_name(usr.get_personalData()->name);
        personalData->set_surname(usr.get_personalData()->surname);
        personalData->set_patronymic(usr.get_personalData()->patronumic);
        personalData->set_yearsold(usr.get_personalData()->yearsold);

        user_impl->set_login(usr.get_login());
        user_impl->set_password(usr.get_pass());
        user_impl->set_access_lvl((UserApi::User_AccessLvl)usr.get_access_lvl());
        user_impl->set_allocated_personal_data(personalData);

        response->set_allocated_user(user_impl);
        response->set_allocated_msg(serverInfo);
        response->set_isregister(true);

        LOG_INFO("-------------> Complete registration!");

        return Status::OK;
    }
}

