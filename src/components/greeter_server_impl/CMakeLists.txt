#---------------------SET COMPONENT NAME---------------------------------------#
set(LIB_NAME greeter_server_impl)
#------------------------------------------------------------------------------#
#---------------------SET LOCAL NAME TO LINK COMPONENT-------------------------#
set(CMAKE_MODULE_PATH ${CMAKE_MODULE_PATH} "${CMAKE_SOURCE_DIR}/cmake/")
set(LOCAL_LOGGER logger)
set(STORAGE storage_lib)
#------------------------------------------------------------------------------#
#---------------------SET PROTO FILE-------------------------------------------#
set(PROTOS
        ${PROJECT_SOURCE_DIR}/protos/message_kernel.proto
        )
set(PROTO_SRC_DIR ${CMAKE_CURRENT_BINARY_DIR}/proto-src)
message(${PROTO_SRC_DIR})
set(Boost_USE_STATIC_LIBS ON)
#------------------------------------------------------------------------------#
#-----------------------INCLUDE HEADER-----------------------------------------#
file(GLOB HEADER_LIST CONFIGURE_DEPENDS
        "${NESTING_CLIENT_SOURCE_DIR}/includes/components/greeter_server_impl/*.h"
        /libraries/logger/logger.h)
list(APPEND CMAKE_PREFIX_PATH "/opt/grpc" "/opt/protobuf")
list(APPEND CMAKE_MODULE_PATH ${PROJECT_SOURCE_DIR}/cmake)

file(MAKE_DIRECTORY ${PROTO_SRC_DIR})
include_directories(${PROTO_SRC_DIR})
#------------------------------------------------------------------------------#
#---------------------FIND PACKAGES--------------------------------------------#
find_package(Protobuf REQUIRED)
find_package(GRPC REQUIRED)
find_package(Threads REQUIRED)
find_package(Boost 1.65 REQUIRED COMPONENTS)
#------------------------------------------------------------------------------#
#---------------------GENERATE AUTOCODE----------------------------------------#
protobuf_generate_cpp(PROTO_SRCS PROTO_HDRS ${PROTO_SRC_DIR} ${PROTOS})
grpc_generate_cpp(GRPC_SRCS GRPC_HDRS ${PROTO_SRC_DIR} ${PROTOS})
#------------------------------------------------------------------------------#
#---------------------COMPILE DEFINES------------------------------------------#
add_definitions(-DBOOST_LOG_DYN_LINK)
add_definitions(-DBOOST_ERROR_CODE_HEADER_ONLY)
#------------------------------------------------------------------------------#
#---------------------ADD SOURCE-----------------------------------------------#
set(SOURCE Server.cpp )
#------------------------------------------------------------------------------#
#---------------------ADD LIBRARY----------------------------------------------#
add_library(${LIB_NAME} ${SOURCE}
        ${HEADER_LIST}
        ${PROTO_SRCS}
        ${GRPC_SRCS})
#------------------------------------------------------------------------------#
#--------------------INCLUDE DIRS----------------------------------------------#
if(Boost_FOUND)
    message(boost found)
    include_directories(${Boost_INCLUDE_DIRS})
endif()
target_include_directories(${LIB_NAME}
        PUBLIC
        ${PROJECT_SOURCE_DIR}/includes/components)
#---------------------LINK COMPONENTS------------------------------------------#
#------------------------------------------------------------------------------#
target_link_libraries(${LIB_NAME}
        gRPC::grpc++_reflection
        protobuf::libprotobuf
        )
target_link_libraries(${LIB_NAME} ${LOCAL_LOGGER})
target_link_libraries(${LIB_NAME} ${STORAGE})
target_link_libraries(${LIB_NAME} ${Boost_LIBRARIES})
#------------------------------------------------------------------------------#
target_compile_features(${LIB_NAME} PUBLIC cxx_std_17)
source_group(TREE "../include" PREFIX "Header Files" FILES ${HEADER_LIST})
