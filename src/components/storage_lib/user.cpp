//
// Created by root on 16.08.21.
//

#include <user.h>

namespace user {

    User::User(std::string login, std::string pass, AccessLvl accessLvl, PersonalData pd)
    {
        _login = login;
        _password = pass;
        _accessLvl = accessLvl;
        _personalData = new PersonalData(pd);
    }

    User::User(User &user) {
        _login = user._login;
        _password = user._password;
        _accessLvl = user._accessLvl;
        _personalData = user._personalData;
    }

    User::User(const User &user) {
        _login = user._login;
        _password = user._password;
        _accessLvl = user._accessLvl;
        _personalData = user._personalData;
    }

    void User::show_data() {
        std::cout << "\n-------------SHOW USER DATA----------------" << std::endl;
        std::cout << "Login: " << _login << "\n";
        std::cout << "Password: " << _password << "\n";
        std::cout << "Access lvl: " << _accessLvl << "\n";
        std::cout << "Name: " << _personalData->name << "\n";
        std::cout << "Surname: " << _personalData->surname << "\n";
        std::cout << "Patronumic: " << _personalData->patronumic << "\n";
        std::cout << "Years old: " << _personalData->yearsold << "\n";
        std::cout << "--------------------------------------------" << "\n\n";
    }

    bool User::AddInDb() {
        LOG_INFO("Data from client:");
        this->show_data(); //DEBUG TODO: delete maybe...
        mongocxx::collection collection = bd::Storage::get_instance()->get_db()[bd::kCollectionName.c_str()];
        auto builder = bsoncxx::builder::stream::document{};

        auto result = collection.find(bsoncxx::builder::stream::document{}
                << "login" << _login << bsoncxx::builder::stream::finalize);
        if(result.begin() != result.end()){
            LOG_ERROR("-------------> Canceled register, user already exist!");
            return false; // If user not unique
        }

        bsoncxx::document::value doc_to_add =
                builder << "id" << _id
                        << "login" << _login
                        << "password" << _password
                        << "access_lvl" << _accessLvl
                        << "personal_data" << bsoncxx::builder::stream::open_document
                            << "name" << _personalData->name
                            << "surname" << _personalData->surname
                            << "patronumic" << _personalData->patronumic
                            << "yearsold" << _personalData->yearsold
                        << bsoncxx::builder::stream::close_document
                        << bsoncxx::builder::stream::finalize;

        collection.insert_one(doc_to_add.view());
        return true;
    }

    bool User::RemoveFromDb() { //TODO NEED TEST
        mongocxx::collection collection = bd::Storage::get_instance()->get_db()[bd::kCollectionName.c_str()];
        auto builder = bsoncxx::builder::stream::document{};
        bsoncxx::document::value doc_for_delete = builder
        << "login" << _login
        << "password" << _password << bsoncxx::builder::stream::finalize;

        auto result = collection.find(doc_for_delete.view());

        if(result.begin() == result.end())
            return false; // User not found

        collection.delete_one(doc_for_delete.view());
        return true;
    }

    bool UpdateInDb(User& current_usr, User& update_usr) { //TODO NEED TEST
        mongocxx::collection collection = bd::Storage::get_instance()->get_db()[bd::kCollectionName.c_str()];
        auto builder = bsoncxx::builder::stream::document{};
        bsoncxx::document::value doc_find({});

        doc_find = builder
                << "login" << current_usr.get_login()
                << "password" << current_usr.get_pass()
                << bsoncxx::builder::stream::finalize;
        auto result_curr_usr = collection.find(doc_find.view());

        doc_find = builder
                << "login" << update_usr.get_login()
                << "password" << update_usr.get_pass()
                << bsoncxx::builder::stream::finalize;
        auto result_upd_usr = collection.find(doc_find.view());

        if(result_curr_usr.begin() == result_curr_usr.end()) {
            LOG_ERROR("-------------> Canceled update, user not found!");
            return false; // User not found
        }

        if(result_upd_usr.begin() != result_upd_usr.end()) {
            LOG_ERROR("-------------> Canceled update, user already exist!");
            return false; //User already exist
        }

            collection.update_one(
            builder
            << "id" << current_usr.get_id()
            << "login" << current_usr.get_login()
            << "password" << current_usr.get_pass()
            << "access_lvl" << current_usr.get_access_lvl()
            << "personal_data" << bsoncxx::builder::stream::open_document
            << "name" << current_usr.get_personalData()->name
            << "surname" << current_usr.get_personalData()->surname
            << "patronumic" << current_usr.get_personalData()->patronumic
            << "yearsold" << current_usr.get_personalData()->yearsold
            << bsoncxx::builder::stream::close_document
            << bsoncxx::builder::stream::finalize,
            builder << "$set" << bsoncxx::builder::stream::open_document
            << "id" << update_usr.get_id()
            << "login" << update_usr.get_login()
            << "password" << update_usr.get_pass()
            << "access_lvl" << update_usr.get_access_lvl()
            << "personal_data" << bsoncxx::builder::stream::open_document
            << "name" << update_usr.get_personalData()->name
            << "surname" << update_usr.get_personalData()->surname
            << "patronumic" << update_usr.get_personalData()->patronumic
            << "yearsold" << current_usr.get_personalData()->yearsold
            << bsoncxx::builder::stream::close_document
            << bsoncxx::builder::stream::close_document
            << bsoncxx::builder::stream::finalize);
        return true;
    }


    PersonalData* User::get_personalData() {
        return _personalData;
    }

    std::string User::get_login() {
        return _login;
    }

    std::string User::get_pass() {
        return _password;
    }
    AccessLvl User::get_access_lvl() {
        return _accessLvl;
    }
    int User::get_id() {
        return _id;
    }
}

